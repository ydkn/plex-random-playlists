package main

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/ydkn/plex-random-playlists/internal/common"
	plexplaylists "gitlab.com/ydkn/plex-random-playlists/internal/playlist"
	"go.uber.org/zap"
)

const (
	plexURLDefault     = "https://localhost:32400"
	plexTokenDefault   = ""
	maxDurationDefault = 48 * time.Hour
	playlistDefault    = ""
	maxEpisodesDefault = 4
	logLevelDefault    = "info"
)

func main() {
	cobra.OnInitialize(func() {
		viper.AutomaticEnv()
	})

	rootCmd := &cobra.Command{
		Use: "plex-random-playlists",
		Run: func(_ *cobra.Command, _ []string) {
			if err := run(); err != nil {
				fmt.Println(err.Error()) //nolint:forbidigo

				os.Exit(1)
			}
		},
	}

	rootCmd.PersistentFlags().String("plex-url", plexURLDefault, "Plex URL")
	_ = viper.BindPFlag("plex_url", rootCmd.PersistentFlags().Lookup("plex-url"))
	viper.SetDefault("plex_url", rootCmd.PersistentFlags().Lookup("plex-url").DefValue)

	rootCmd.PersistentFlags().String("plex-token", plexTokenDefault, "Plex Token")
	_ = viper.BindPFlag("plex_token", rootCmd.PersistentFlags().Lookup("plex-token"))
	viper.SetDefault("plex_token", rootCmd.PersistentFlags().Lookup("plex-token").DefValue)

	rootCmd.PersistentFlags().Duration("max-duration", maxDurationDefault, "Maximum duration of playlist")
	_ = viper.BindPFlag("max_duration", rootCmd.PersistentFlags().Lookup("max-duration"))
	viper.SetDefault("max_duration", rootCmd.PersistentFlags().Lookup("max-duration").DefValue)

	rootCmd.PersistentFlags().StringSlice("libraries", []string{}, "Libraries")
	_ = viper.BindPFlag("libraries", rootCmd.PersistentFlags().Lookup("libraries"))
	viper.SetDefault("libraries", rootCmd.PersistentFlags().Lookup("libraries").DefValue)

	rootCmd.PersistentFlags().StringSlice("genres", []string{}, "genres")
	_ = viper.BindPFlag("genres", rootCmd.PersistentFlags().Lookup("genres"))
	viper.SetDefault("genres", rootCmd.PersistentFlags().Lookup("genres").DefValue)

	rootCmd.PersistentFlags().StringSlice("labels", []string{}, "labels")
	_ = viper.BindPFlag("labels", rootCmd.PersistentFlags().Lookup("labels"))
	viper.SetDefault("labels", rootCmd.PersistentFlags().Lookup("labels").DefValue)

	rootCmd.PersistentFlags().String("playlist", playlistDefault, "Playlist")
	_ = viper.BindPFlag("playlist", rootCmd.PersistentFlags().Lookup("playlist"))
	viper.SetDefault("playlist", rootCmd.PersistentFlags().Lookup("playlist").DefValue)

	rootCmd.PersistentFlags().Uint64("max-consecutive-episodes", maxEpisodesDefault, "Maximum consecutive episodes")
	_ = viper.BindPFlag("max_consecutive_episodes", rootCmd.PersistentFlags().Lookup("max-consecutive-episodes"))
	viper.SetDefault("max_consecutive_episodes", rootCmd.PersistentFlags().Lookup("max-consecutive-episodes").DefValue)

	rootCmd.PersistentFlags().String("log-level", logLevelDefault, "log level")
	_ = viper.BindPFlag("log_level", rootCmd.PersistentFlags().Lookup("log-level"))
	viper.SetDefault("log_level", rootCmd.PersistentFlags().Lookup("log-level").DefValue)

	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err) //nolint:forbidigo
		os.Exit(1)
	}
}

func run() error {
	logger, err := common.NewLogger(viper.GetString("log_level"))
	if err != nil {
		return fmt.Errorf("faild to create logger: %w", err)
	}

	defer func() { _ = logger.Sync() }()

	generator, err := newGenerator(logger)
	if err != nil {
		return err
	}

	err = generator.Run()
	if err != nil {
		return fmt.Errorf("failed to generate playlist: %w", err)
	}

	return nil
}

func newGenerator(log *zap.SugaredLogger) (*plexplaylists.Generator, error) {
	//nolint:wrapcheck
	return plexplaylists.NewGenerator(plexplaylists.GeneratorConfig{
		Log:                    log.Named("generator"),
		PlexURL:                viper.GetString("plex_url"),
		PlexToken:              viper.GetString("plex_token"),
		MaxDuration:            viper.GetDuration("max_duration"),
		Libraries:              viperGetStringSlice("libraries"),
		Genres:                 viperGetStringSlice("genres"),
		Labels:                 viperGetStringSlice("labels"),
		Playlist:               viper.GetString("playlist"),
		MaxConsecutiveEpisodes: viper.GetUint64("max_consecutive_episodes"),
	})
}

func viperGetStringSlice(name string) []string {
	items := make([]string, 0)

	for _, viperValue := range viper.GetStringSlice(name) {
		for _, item := range strings.Split(strings.TrimSpace(viperValue), ",") {
			if item != "[]" && item != "" {
				items = append(items, item)
			}
		}
	}

	return items
}
