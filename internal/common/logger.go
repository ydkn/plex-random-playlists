package common

import (
	"errors"
	"fmt"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var ErrInvalidLogLevel = errors.New("invalid log level")

// NewLogger instance a new logger with given log level.
func NewLogger(logLevel string) (*zap.SugaredLogger, error) {
	logConfig := zap.NewProductionConfig()

	if logLevel == "" {
		logLevel = "info"
	}

	switch logLevel {
	case "debug":
		logConfig.Level = zap.NewAtomicLevelAt(zapcore.DebugLevel)
	case "info":
		logConfig.Level = zap.NewAtomicLevelAt(zapcore.InfoLevel)
	case "warn":
		logConfig.Level = zap.NewAtomicLevelAt(zapcore.WarnLevel)
	case "error":
		logConfig.Level = zap.NewAtomicLevelAt(zapcore.ErrorLevel)
	case "panic":
		logConfig.Level = zap.NewAtomicLevelAt(zapcore.PanicLevel)
	default:
		return nil, ErrInvalidLogLevel
	}

	logConfig.EncoderConfig.TimeKey = "ts"
	logConfig.EncoderConfig.LevelKey = "lvl"
	logConfig.EncoderConfig.NameKey = "module"
	logConfig.EncoderConfig.CallerKey = ""

	logger, err := logConfig.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to initialize logger: %w", err)
	}

	return logger.Sugar(), nil
}
