package plex

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"runtime"
	"strings"
	"time"

	"gitlab.com/ydkn/plex-random-playlists/internal/common"
	"go.uber.org/zap"
)

const (
	version     = "0.1.0"
	httpTimeout = 10 * time.Second
)

type ClientConfig struct {
	Log   *zap.SugaredLogger
	URL   string
	Token string
}

type Client struct {
	log        *zap.SugaredLogger
	url        string
	token      string
	httpClient http.Client
	headers    map[string]string
	info       *baseAPIResponse
}

func New(config ClientConfig) (*Client, error) {
	if config.Log == nil {
		return nil, common.ErrNoLogger
	}

	client := &Client{
		log:        config.Log,
		url:        config.URL,
		token:      config.Token,
		httpClient: http.Client{Timeout: httpTimeout},
		headers: map[string]string{
			"Accept":                   "application/json",
			"X-Plex-Platform":          runtime.GOOS,
			"X-Plex-Platform-Version":  version,
			"X-Plex-Client-Identifier": "go-plex-client-v" + version,
			"X-Plex-Product":           "Go Plex Client",
			"X-Plex-Version":           version,
			"X-Plex-Device":            fmt.Sprintf("%s %s", runtime.GOOS, runtime.GOARCH),
			"X-Plex-Token":             config.Token,
		},
	}

	info, err := client.getInfo()
	if err != nil {
		return nil, err
	}

	client.info = info

	return client, nil
}

func (c *Client) getInfo() (*baseAPIResponse, error) {
	statusCode, body, err := c.httpRequest(http.MethodGet, "/")
	if err != nil {
		return nil, err
	}

	if statusCode != http.StatusOK {
		return nil, errRequestFailed
	}

	var result baseAPIResponse

	if err := c.parseResponse(body, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

func (c *Client) URIRoot() string {
	return fmt.Sprintf("server://%s/com.plexapp.plugins.library", c.info.MediaContainer.MachineIdentifier)
}

func (c *Client) GetLibraries() ([]*Library, error) {
	statusCode, body, err := c.httpRequest(http.MethodGet, "/library/sections")
	if err != nil {
		return nil, err
	}

	if statusCode != http.StatusOK {
		return nil, errRequestFailed
	}

	var result libraryResponse

	if err := c.parseResponse(body, &result); err != nil {
		return nil, err
	}

	return result.MediaContainer.Directory, nil
}

func (c *Client) GetLibraryContent(key string) ([]*Metadata, error) {
	statusCode, body, err := c.httpRequest(http.MethodGet, fmt.Sprintf("/library/sections/%s/all", key))
	if err != nil {
		return nil, err
	}

	if statusCode != http.StatusOK {
		return nil, errRequestFailed
	}

	var result containerResult

	if err := c.parseResponse(body, &result); err != nil {
		return nil, err
	}

	return c.convertMetadatas(result.MediaContainer.Metadata), nil
}

func (c *Client) GetLibraryMetadata(key string) ([]*Metadata, error) {
	statusCode, body, err := c.httpRequest(http.MethodGet, "/library/metadata/"+key)
	if err != nil {
		return nil, err
	}

	if statusCode != http.StatusOK {
		return nil, errRequestFailed
	}

	var result containerResult

	if err := c.parseResponse(body, &result); err != nil {
		return nil, err
	}

	return c.convertMetadatas(result.MediaContainer.Metadata), nil
}

func (c *Client) GetChildren(key string) ([]*Metadata, error) {
	statusCode, body, err := c.httpRequest(http.MethodGet, fmt.Sprintf("/library/metadata/%s/children", key))
	if err != nil {
		return nil, err
	}

	if statusCode != http.StatusOK {
		return nil, errRequestFailed
	}

	var result containerResult

	if err := c.parseResponse(body, &result); err != nil {
		return nil, err
	}

	return c.convertMetadatas(result.MediaContainer.Metadata), nil
}

func (c *Client) GetPlaylists() ([]*Metadata, error) {
	statusCode, body, err := c.httpRequest(http.MethodGet, "/playlists")
	if err != nil {
		return nil, err
	}

	if statusCode != http.StatusOK {
		return nil, errRequestFailed
	}

	var result containerResult

	if err := c.parseResponse(body, &result); err != nil {
		return nil, err
	}

	return c.convertMetadatas(result.MediaContainer.Metadata), nil
}

func (c *Client) GetPlaylistItems(key string) ([]*Metadata, error) {
	statusCode, body, err := c.httpRequest(http.MethodGet, fmt.Sprintf("/playlists/%s/items", key))
	if err != nil {
		return nil, err
	}

	if statusCode != http.StatusOK {
		return nil, errRequestFailed
	}

	var result containerResult

	if err := c.parseResponse(body, &result); err != nil {
		return nil, err
	}

	return c.convertMetadatas(result.MediaContainer.Metadata), nil
}

func (c *Client) CreatePlaylist(title string, listType string) (*Metadata, error) {
	query, err := url.Parse("/playlists")
	if err != nil {
		return nil, fmt.Errorf("%w: %s", errRequestParsingFailed, err.Error())
	}

	queryValues := query.Query()
	queryValues.Add("title", title)
	queryValues.Add("type", listType)
	queryValues.Add("uri", "/library/metadata/")
	queryValues.Add("smart", "0")

	query.RawQuery = queryValues.Encode()

	statusCode, body, err := c.httpRequest(http.MethodPost, query.String())
	if err != nil {
		return nil, err
	}

	if statusCode != http.StatusOK {
		return nil, errRequestFailed
	}

	var result containerResult

	if err := c.parseResponse(body, &result); err != nil {
		return nil, err
	}

	return c.convertMetadata(result.MediaContainer.Metadata[0]), nil
}

func (c *Client) AddPlaylistItems(key string, items []*Metadata) (*Metadata, error) {
	query, err := url.Parse(fmt.Sprintf("/playlists/%s/items", key))
	if err != nil {
		return nil, fmt.Errorf("%w: %s", errRequestParsingFailed, err.Error())
	}

	uriRoot := ""
	itemKeys := make([]string, len(items))

	for i, item := range items {
		uriRoot = item.uriRoot
		itemKeys[i] = item.Key
	}

	queryValues := query.Query()
	queryValues.Add("uri", fmt.Sprintf("%s/library/metadata/%s", uriRoot, strings.Join(itemKeys, ",")))

	query.RawQuery = queryValues.Encode()

	statusCode, body, err := c.httpRequest(http.MethodPut, query.String())
	if err != nil {
		return nil, err
	}

	if statusCode != http.StatusOK {
		return nil, errRequestFailed
	}

	var result containerResult

	if err := c.parseResponse(body, &result); err != nil {
		return nil, err
	}

	return c.convertMetadata(result.MediaContainer.Metadata[0]), nil
}

func (c *Client) RemovePlaylistItem(key string, item *Metadata) (*Metadata, error) {
	query, err := url.Parse(fmt.Sprintf("/playlists/%s/items/%d", key, item.PlaylistItemID))
	if err != nil {
		return nil, fmt.Errorf("%w: %s", errRequestParsingFailed, err.Error())
	}

	statusCode, body, err := c.httpRequest(http.MethodDelete, query.String())
	if err != nil {
		return nil, err
	}

	if statusCode != http.StatusOK {
		return nil, errRequestFailed
	}

	var result containerResult

	if err := c.parseResponse(body, &result); err != nil {
		return nil, err
	}

	return c.convertMetadata(result.MediaContainer.Metadata[0]), nil
}

func (c *Client) httpRequest(method string, query string) (int, []byte, error) {
	ctx, ctxCancel := context.WithTimeout(context.Background(), c.httpClient.Timeout)
	defer ctxCancel()

	req, err := http.NewRequestWithContext(ctx, method, fmt.Sprintf("%s%s", c.url, query), nil)
	if err != nil {
		return 0, nil, fmt.Errorf("%w: %s", errRequestParsingFailed, err.Error())
	}

	for k, v := range c.headers {
		req.Header.Add(k, v)
	}

	resp, err := c.httpClient.Do(req)
	if err != nil {
		return 0, nil, fmt.Errorf("%w: %s", errRequestFailed, err.Error())
	}

	defer resp.Body.Close()

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, nil, fmt.Errorf("%w: %s", errResponseParsingFailed, err.Error())
	}

	return resp.StatusCode, data, nil
}

func (c *Client) parseResponse(data []byte, result interface{}) error {
	fixedData := []byte(strings.ReplaceAll(string(data), `"Guid"`, `"Guids"`))

	if err := json.Unmarshal(fixedData, result); err != nil {
		return fmt.Errorf("%w: %s", errResponseParsingFailed, err.Error())
	}

	return nil
}

func (c *Client) convertMetadata(m *metadataJSON) *Metadata {
	metadata := m.metadata()

	metadata.uriRoot = c.URIRoot()

	return &metadata
}

func (c *Client) convertMetadatas(ms []*metadataJSON) []*Metadata {
	metadatas := make([]*Metadata, len(ms))

	for i, m := range ms {
		metadatas[i] = c.convertMetadata(m)
	}

	return metadatas
}
